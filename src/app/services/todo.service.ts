import { Injectable } from '@angular/core';
import { Todo } from '../models/todo.model';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class TodoService {
private _url = '/assets/data/Todos.json';

  constructor(private http: HttpClient) { }
  getToDos() {
    return this.http.get<Todo[]>(this._url).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server error');
  }
}
