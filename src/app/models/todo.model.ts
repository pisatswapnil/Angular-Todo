export class Todo {
  title: string;
  id: number;
  completed: boolean;
  edit: boolean;
  bookmark: boolean;
  createdOn: Date;

  constructor(todo) {
    this.title = todo.title || 'todo';
    this.id = todo.id;
    this.completed = todo.completed || false;
    this.edit = todo.edit || false;
    this.bookmark = todo.bookmark || false;
    this.createdOn = todo.createdOn || new Date();
  }
}
