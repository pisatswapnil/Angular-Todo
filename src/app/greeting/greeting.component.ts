import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.css']
})
export class GreetingComponent implements OnInit {

  public message = 'Some default greeting.';
  public error = true;
  public successClass = 'text-success';
  public highlightColor = 'yellow';
  public modelGreeting = '';
  public messageClasses = {
    'text-success': !this.error,
    'text-error': this.error
  };
  public titleClasses = {
    'color': 'blue',
    'font-style': 'italic'
  };
  constructor() { }

  ngOnInit() {
  }

  onClick(greeting) {
    this.modelGreeting = 'From class file';
    this.message = greeting;
  }

}
