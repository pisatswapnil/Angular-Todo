import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { GreetingComponent } from './greeting/greeting.component';
import { AboutComponent } from './feature/about/about.component';
import { TodolistComponent } from './feature/todolist/todolist.component';
import { EllipsisPipe } from './shared/ellipsis.pipe';
import { ModelComponent } from './shared/model/model.component';
import { LoginComponent } from './feature/login/login.component';
import { TodoService} from './services/todo.service';
import { RegisterComponent } from './feature/register/register.component';
import { DataTableComponent } from './shared/data-table/data-table.component';


const appRoutes: Routes = [
  {path: '', component: TodolistComponent},
  {path: 'about', component: AboutComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GreetingComponent,
    AboutComponent,
    TodolistComponent,
    EllipsisPipe,
    ModelComponent,
    LoginComponent,
    RegisterComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(
    appRoutes,
    { enableTracing: true}
  )
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
