import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {};

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onLogin() {
    if (this.user.userName == 'Swapnil') {
      this.router.navigate(['/']);
    }
  }
  debug(f) {
  return JSON.stringify(f.value);
  }

}
