import { Component, OnInit } from '@angular/core';
import {Todo} from '../../models/todo.model';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  droppedTodo: any;
  error: any;


  title = 'app';

  todos = [];
  filteredTodos = [];
  filterAction = 'ALL';
  todoModal = null;
  isModal = false;
  completedTodo = [];

  constructor(private todoService: TodoService) {

  }
  ngOnInit() {
   this.todoService.getToDos()
  .subscribe(data =>  {
     this.todos = data;
     this.filteredTodos = [...this.todos];
    }, error =>  {
      this.error = error;
    });

  }

  addToDo(newTodo: HTMLInputElement) {
    const toDo = new Todo({
      id: this.todos.length + 1,
      title: newTodo.value,
      completed: false,
      edit: false,
      bookmark: false
    });

    this.todos.push(toDo);
    this.onFilterAction('ALL');
  }

  deleteTodo(id) {
    const modifiedList = this.todos.filter((todo) =>  {
      return todo.id !== id;
    });
    this.todos = modifiedList;
    this.onFilterAction('ALL');
  }

  log(...args) {
    for (let i = 0; i < args.length; i++) {
      console.log(JSON.stringify(args[i]));
    }
  }
  toggleEdit(id) {
    const todoEdit = this.todos.find((todo) =>  {
      return todo.id === id;
    });
    todoEdit.edit = !todoEdit.edit;
    this.log(todoEdit);
  }
  editTodo(event, id, dirtyToDo) {
    if (event.which === 27) {
      this.toggleEdit(id);
      return;
    }

    if (event.which === 13) {
      const found = this.findToDO(id);
      found.title = dirtyToDo;
      this.toggleEdit(id);
      return;
    }
  }

  findToDO(id: number) {
    const numId = +id;
    const todoEdit = this.todos.find((todo) =>  {
          return todo.id === numId;
        });
    return todoEdit;
  }

  toggleBookmark(id) {
    const found = this.findToDO(id);
    found.bookmark = !found.bookmark;
    this.log(found);
    this.onFilterAction('ALL');
  }

  toggleCompleted(id) {
    const found = this.findToDO(id);
    found.completed = !found.completed;
    this.log(found);
    this.onFilterAction('ALL');
  }
  onFilterChange(event) {
    if (!event.target.name) { return; }
    const action = event.target.name.trim().toUpperCase();
    this.filterAction = action;
    this.onFilterAction(action);
  }


  onFilterAction(action) {

    switch (action) {
      case 'ALL':
        this.filteredTodos = [...this.todos];
        break;
        case 'COMPLETED':
        this.filteredTodos = this.todos.filter((todo) =>  {
          return todo.completed;
        });
        break;
        case 'BOOKMARKED':
        this.filteredTodos = this.todos.filter((todo) =>  {
          return todo.bookmark;
        });
        break;
      default:
        break;
    }
  }

  showModal(id) {
    const found = this.findToDO(id);
    this.todoModal = found;
    this.isModal = true;
  }
  onClose(event) {
    this.isModal = event;
  }
  onDragStart(event, id) {
    event.dataTransfer.setData('todoid', id);
  }
  onDragover(event) {
    event.preventDefault();
  }
  onDrop(event) {
    const id = event.dataTransfer.getData('todoid');
    const todo = this.findToDO(id);
    todo.completed = true;
    this.completedTodo.push(todo);
  }

}
